<?php 
    session_start();
    if($_SESSION['loggedin'] != 'loggedin')
    {
        header('Location: login.php');
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard</title>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
    <style type="text/css">
        .sub_box
        {
            position: relative;
            width: 100%;
            height: 150px;
            border-radius: 15px 15px;
            border: 1px solid #f1f1f1;
            background-color: #f0ad4e;
            text-align: center;
            color: #d9534f;
        }
        
    </style>
</head>
<body>
    <header>

    </header>
    <div class="container">
        <div>
            <h1>ADMIN DASHBORAD</h1>
            <hr/>
            <a href="logout.php">Logout</a>
        </div>
        <div class="row">
            <div class="col-md-4">
                <a href="student.php">
                    <div class="sub_box">
                        <h2><span class="glyphicon glyphicon-education"></span> Student</h2>
                    </div>
                </a>
            </div> 
            <div class="col-md-4">
                <div class="sub_box">
                <h2><span class="glyphicon glyphicon-education"></span> Rooms</h2>
                </div>
            </div>
            <div class="col-md-4">
                <div class="sub_box">
                <h2><span class="glyphicon glyphicon-education"></span> Registration</h2>
                </div>
            </div>
        </div>
    </div>
    <footer></footer>
</body>
</html>