<?php

use Mockery\Generator\StringManipulation\Pass\Pass;

    include("../../connection.php");
    $conn =connection();
    session_start();
    if(isset($_GET['submit']))
    {
        $usr = $_GET['user_name'];
        $pwd = $_GET['pwd'];
      
        $sql = "SELECT * FROM tbl_user WHERE user_name ='$usr'";
        $data = $conn->query($sql);
    
        if($data)
        {     while($row = mysqli_fetch_assoc($data)){
                $get_pwd = $row['password'];
                $chk = password_verify($pwd, $get_pwd);
                if($chk)
                {
                    $_SESSION['loggedin'] ='loggedin';
                    header('Location: index.php');
                }
            }
            
        }
        // header('Location: index.php');
        
        
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin login</title>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
</head>
<body>
    <div class="container">
        <div>
            <h2>User login</h2>
        </div>
        <div class="row">
            <div class="col-md-4">
            <form method="get">
                <div class="form-group">
                    <label for="user_name">User Name</label>
                    <input name="user_name" type="text" class="form-control" id="user_name" >
                </div>
                <div class="form-group">
                    <label for="pwd">Password</label>
                    <input name="pwd" type="password" class="form-control" id="pwd">
                </div>
                <div>
                    <a href="register.php">Register</a>
                </div>
               
                <button type="submit" class="btn btn-default" name="submit">Submit</button>
            </form>
            </div>
        </div>
    </div>
</body>