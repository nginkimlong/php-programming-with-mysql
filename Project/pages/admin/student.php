<?php 
    session_start();
    if($_SESSION['loggedin'] != 'loggedin')
    {
        header('Location: login.php');
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Student page</title>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
</head>
<body>

    <script type="text/javascript">
        function search_student()
        {
            var key= document.getElementById("key_term_search").value;
            $.ajax({
                url: 'search_student.php', 
                method: "GET",
                data: {key: key},
                success: function (data) {
                    console.log("success");
                    document.getElementById("block_student").innerHTML = data;
                },error:function(data)
                {
                    console.log("Error: ");
                    console.log(data);
                }
               
            });
        }
    </script>

    <header>

    </header>
    <div class="container">
        <div>
            <h1>Student Information</h1>
        </div>
        <div style="width: 100%;">
            <div style="width: 45%; float:left;">
                <input type="text" name="key_term_search" id="key_term_search"/> <button onclick="search_student()">Search</search>
            </div>
            <div style="width: 45%; float:right;">
                <div style="text-align: right;"><a href="add_new.php" class="btn btn-primary"> <span class="glyphicon glyphicon-plus"></span> Add new</a></div>
            </div>
        </div>
        
        <div id="block_student">
            <table class="table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Sex</th>
                        <th>DOB</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $n = 1;
                        include("../../connection.php");
                        $conn =connection();
                        $sql = "SELECT * FROM tbl_student";
                        $data = $conn->query($sql);
                        if($data)
                        {
                            while($row = mysqli_fetch_assoc($data)){
                                    ?>
                                <tr>
                                    <td><?php echo $n; ?></td>
                                    <td><?php echo $row['first_name']; ?></td>
                                    <td><?php echo $row['last_name']; ?></td>
                                    <td><?php echo $row['sex']; ?></td>
                                    <td><?php echo $row['dob']; ?></td>
                                    <td>
                                        <a href="student_edit.php?id=<?php echo $row['id']; ?>&test='hell world'" class="btn btn-info"><span class="glyphicon glyphicon-pencil"></span> Edit</a>
                                    </td>
                                </tr>
                            <?php
                            $n = $n + 1;
                            }
                        }
                    ?>
                </tbody>

            </table>
        </div>
       
    </div>

    <footer>

    </footer>
    
    
</body>
</html>

