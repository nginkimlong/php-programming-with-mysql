
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style type="text/css">
        ul{
            list-style-type:  none;
        }
        ul li 
        {
            display: inline;
        }
        ul li a{
            background-color: blue;
            color:white;
            padding: 15px 10px;
            border-radius: 5px 5px;
            text-decoration: none;
        }
        ul li a:hover{
            background-color: pink;
        }
    </style>
</head>
<body>
    <ul>
    <?php 
        $arr_menu = array('index.php' => "Home", 
        "product.php"=>"Product", "service.php" => "Service", 
        'contact.php'=>'Contact', 'about.php' => "About");
        foreach ($arr_menu as $key => $value) {
            ?>
                <li><a href="<?php echo $key; ?>"><?php echo $value; ?></a></li>
        <?php
            }
            // echo $arr_menu['index.php'];
        ?>
    </ul>
</body>
</html>
