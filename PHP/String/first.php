<?php 
    $str1 = "GreatX Center";
    $str2 = "Teaching online";

    $main_str = $str1." ".$str2;
    echo "Hello $main_str";
   
    // Escape character 
    $my_string = 'I\'m teaching online <br/>';
    echo $my_string;

    $drive = 'C:\\\my_computer\\\mywork<br/>';
    echo $drive;

    $price = 1200;
    echo "Price is $price ";

    // word count
    var_dump(str_word_count($str2));

    // Change string 
    $s_string = strtolower($str1);
    echo "<br/>".$s_string;

    $c_string = strtoupper($str1);
    echo "<br/>".$c_string;

    $uc_first = ucfirst($str1);
    echo "<br/>".$uc_first;

    $lc_first = lcfirst($str1);
    echo "<br/>$lc_first";

    $uc_word = ucwords($str1);
    echo "<br/> $uc_word";





?>