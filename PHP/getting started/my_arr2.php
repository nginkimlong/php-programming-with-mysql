<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style type="text/css">
        ul{
            list-style-type: none;
        }
        ul li 
        {
            display: inline;
        }
        ul li a 
        {
            text-decoration: none;
            background-color: blue;
            padding: 15px;
            border-radius: 5px 5px;
            color: white;
        }
        ul li a:hover
        {
            background-color: red;
        }

    </style>
</head>

<body>
    <?php 
        $menu = array('index.php'=>'Home', 'product.php'=>"Product", 'service.php'=>"Services", 'contact.php'=>'Contact', 'about.php'=>"About");
        // echo $menu['index.php'];
        // echo $menu['product.php'];
        ?>
        <ul>

            <?php
                foreach ($menu as $key => $value) {
                    echo "<li><a href='$key'>$value</a> </li>";
                }
            ?>
        </ul>
    <?php
    ?>
</body>
</html>