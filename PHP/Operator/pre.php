<?php 
    $i = 1; 
    // $i = $i + 1; 
    // echo $i;

    //  prefix 
    $result = ++$i; #$i = $i + 1;
    echo "Result one: ", $result, "<br/>";

    $result = ++$i; #$i = $i + 1;
    echo "Result two: ", $result, "<br/>";

    $result = ++$i; #$i = $i + 1;
    echo "Result three: ", $result, "<br/>";


    // Postfix
    $x = 1;
    $r1 = $x++;
    echo "Result one is: ", $r1, "<br/>";
    $r2 = $x++;
    echo "Result one is: ", $r2, "<br/>";
    $r3 = $x++;
    echo "Result one is: ", $r3, "<br/>";


?>