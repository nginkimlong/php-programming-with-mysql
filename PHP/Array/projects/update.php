<?php 
    $data = $_GET["value"];
    $new_array = $_GET['arr_student'];

    $new_array[$data] = ["010", "Change Name", "Female", 50, "KPC"];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
    <h1>Change student <?php echo $data; ?></h1>
    <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Student Name</th>
                <th>Sex</th>
                <th>Age</th>
                <th>Birth place</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
                foreach ($new_array as $key => $value) {
                    ?>
                        <tr>
                            <td><?php echo $value[0]; ?></td>
                            <td><?php echo $value[1]; ?></td>
                            <td><?php echo $value[2]; ?></td>
                            <td><?php echo $value[3]; ?></td>
                            <td><?php echo $value[4]; ?></td>
                            <td>
                                <button id="id_edit" onclick="edit_value('<?php echo $key; ?>')">Edit</button>
                                <button id="id_sort">Sort</button>
                                <button id="id_delete">Delete</button>
                                <input type="hidden" value="<?php echo $key ?>" id="txt_id_<?php echo $key; ?>" />
                            </td>
                        </tr>
                    <?php
                }
            ?>
        </tbody>
    </table>
</body>
</html>
