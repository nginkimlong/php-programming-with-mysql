
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script type="text/javascript">
        let a = 10; 
        let b = 20;
        var result = a + b;
        document.getElementById("data").innerHTML = result;
    </script>
</head>
<body>
    <h1>Hello world</h1>
    <div id="data"></div>
</body>
</html>

<?php 
        // Syntax 1 of association array
        $arr_student = array(
            "Sotun"=>["001", "Thay Sothun", "Male", 21, "Bathambong"],
            "Giva"=>["002", "Mai Giva", "Male", 21, "Bantey Meanchey"],
            "Chanta"=>["003", "Chea Chantha", "Female", 25, "Kampong Cham"],
            "Tola"=>["004", "Chan Tola", "Male", 18, "Phnom Penh"],
            "Neardy"=>["005", "Men Neardy", "Female", 22, "Kampong Thom"],
        );
        // Syntax 2 of association array
        $arr_student["Neath"] = ["006", "Vuth Munineath", "Female", 23, "Thboung Khmom"];
        
        $arr_student2 = array(
            "Chan"=>["007", "Mao Channy", "Female", 22, "Kampong Thom"],
        );
        $new_array = array_merge($arr_student, $arr_student2);
           
        ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Student</title>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
    <script type="text/javascript">
       
        function edit_value(my_key)
        {
            const arr = {}
            <?php 
                foreach ($new_array as $key => $value) {
            ?>  
                var arr_item = [];
                arr_item.push('<?php echo $value[0]; ?>', '<?php echo $value[1]; ?>', 
                '<?php echo $value[2]; ?>', <?php echo $value[3]; ?>, '<?php echo $value[4]; ?>');
                arr['<?php echo $key; ?>']  = arr_item;    
            <?php 
                }
            ?>

            $.ajax({
                type: "GET",
                url: "update.php",
                data: {
                    "value": my_key, 
                    "arr_student": arr,
                },
                success: function(data){
                    //if request if made successfully then the response represent the data
                   document.getElementById("content").innerHTML = data;
                }, 
                error: function(error)
                {
                    console.log(error);
                }
            });
        }
        
    </script>

</head>
<body>
    <main class="container">
        

        <h1>Student list</h1>
        <div id="content">
            <table class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Student Name</th>
                        <th>Sex</th>
                        <th>Age</th>
                        <th>Birth place</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        foreach ($new_array as $key => $value) {
                            ?>
                                <tr>
                                    <td><?php echo $value[0]; ?></td>
                                    <td><?php echo $value[1]; ?></td>
                                    <td><?php echo $value[2]; ?></td>
                                    <td><?php echo $value[3]; ?></td>
                                    <td><?php echo $value[4]; ?></td>
                                    <td>
                                        <button id="id_edit" onclick="edit_value('<?php echo $key; ?>')">Edit</button>
                                        <button id="id_delete">Delete</button>
                                        <input type="hidden" value="<?php echo $key ?>" id="txt_id_<?php echo $key; ?>" />
                                    </td>
                                </tr>
                            <?php
                        }
                    ?>
                </tbody>
            </table>
            <hr/>
            <button>Accending Sort</button>
            <button>Decening Sort</button>
        </div>
       





     </main>
    
</body>
</html>