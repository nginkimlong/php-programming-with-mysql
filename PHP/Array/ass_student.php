<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Student</title>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
</head>
<body>
    <main class="container">

        <?php 
            // Syntax 1 of association array
            $arr_student = array(
                "Sotun"=>["001", "Thay Sothun", "Male", 21, "Bathambong"],
                "Giva"=>["002", "Mai Giva", "Male", 21, "Bantey Meanchey"],
                "Chanta"=>["003", "Chea Chantha", "Female", 25, "Kampong Cham"],
                "Tola"=>["004", "Chan Tola", "Male", 18, "Phnom Penh"],
                "Neardy"=>["005", "Men Neardy", "Female", 22, "Kampong Thom"],
            );
            
            // Syntax 2 of association array
            $arr_student["Neath"] = ["006", "Vuth Munineath", "Female", 23, "Thboung Khmom"];
            
            $arr_student2 = array(
                "Chan"=>["007", "Mao Channy", "Female", 22, "Kampong Thom"],
            );

            $new_array = array_merge($arr_student, $arr_student2);
            $cur = current($arr_student);
            echo "<h2>Current student: </h2>";
            echo "<hr/>";
            echo "<p>ID: $cur[0]</p>";
            echo "<p>Name: $cur[1]</p>";
            echo "<p>Sex: $cur[2]</p>";
            echo "<p>Age: $cur[3]</p>";
            echo "<p>Address: $cur[4]</p>";


            // Search key
            $key = "Giva";
            $find_key = array_key_exists($key, $arr_student);
            if($find_key == 1)
            {
                // The use in_array() function
                $find_index = in_array("002", $arr_student[$key]);
                echo "<hr/>";
                echo "Found value of Key name : Sotun<br/>";
                if($find_index==true)
                {
                    echo "Found!";
                }else{
                    echo "Not found";
                }

                // The use array_search function
                $find_key = array_search("Bathambong", $arr_student[$key]);
                echo $find_key;


                // Sort array 
                $my_data = $arr_student["Chanta"];
                sort($my_data);
                // var_dump($my_data);
                echo "<br/>";
                rsort($my_data);
                // var_dump($my_data);


                // Association array sort (value)

                // asort($arr_student);
                // var_dump($arr_student);

                // arsort($arr_student);
                // echo "<br/>";
                // var_dump($arr_student);

                // // Association array sort (key)

                ksort($arr_student);
                // var_dump($arr_student);
                krsort($arr_student);
                var_dump($arr_student);
            }

        ?>
     </main>
    
</body>
</html>