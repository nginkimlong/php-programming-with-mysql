<?php 
    $arr_student = array();
    $first_name = "";
    $last_name = "";
    $sex = "";
    $age = "";
    $email = "";
    $phone = "";
    if(isset($_POST['Submit']))
    {
        $first_name = $_REQUEST['first_name'];
        $last_name = $_REQUEST['last_name'];
        $sex = $_REQUEST['opt_sex'];
        $age = $_REQUEST['txt_age'];
        $email = $_REQUEST['txt_email'];
        $phone = $_REQUEST['txt_tel'];
        $arr_student = array($first_name, $last_name, $sex, $age, $email, $phone);
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>

</head>
<body>
    <div class="container">
        <div>
        <h1><span class="glyphicon glyphicon-king"></span> Student Information</h1>
        </div>
        <form action="" method="POST">
            <div class="form-group">
                <label for="first_name">First Name: </label>
                <input value="<?php echo $first_name; ?>" required type="text" class="form-control" name="first_name" id="first_name" placeholder="Input first name">
            </div>
            <div class="form-group">
                <label for="last_name">Last Name: </label>
                <input value="<?php echo $last_name; ?>" required type="text" class="form-control" name="last_name" id="last_name" placeholder="Input last name">
            </div>
            <div class="form-group">
                <label for="last_name">Sex: </label>
                <select class="form-control" name="opt_sex">
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>
                </select>
            </div>
            <div class="form-group">
                <label for="age">Age: </label>
                <input value="<?php echo $age; ?>" required type="number" class="form-control" name="txt_age" id="txt_age" >
            </div>
            <div class="form-group">
                <label for="txt_email">Email: </label>
                <input value="<?php echo $email; ?>" required type="email" class="form-control" name="txt_email" id="txt_email" >
            </div>
            <div class="form-group">
                <label for="txt_tel">Telephone: </label>
                <input value="<?php echo $phone; ?>" required type="tel" class="form-control" name="txt_tel" id="txt_tel" >
            </div>
            <button type="submit" class="btn btn-success btn-lg" name="Submit">Submit</button> 
            <!-- <input type="submit" value="Submit" /> -->
            <input type="reset" class="btn btn-danger btn-lg" value="Reset">
        </form>

        <div>
            <?php 

                if(!empty($arr_student)){
                    echo "<h2>$first_name $last_name </h2>";
                echo "<p>Age: $age</p>";
                echo "<p>Sex: $sex</p>";
                echo "<p>Email: $email</p>";
                echo "Telephone: $phone";
                // var_dump($arr_student);
                }
                
            ?>
        </div>
    </div>


    
</body>
</html>