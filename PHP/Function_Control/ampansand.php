<?php 

    $n = 10;
    function IncrementByValue($CountByValue){
        ++$CountByValue; // $CountByvalue = $CountByvalue + 1;
        echo "<p> IncrementByValue() value is 	$CountByValue </p>";
    }
    IncrementByValue($n);
    echo " N after calling by value:  $n";


    // ampansand of function, pass by reference 
    function IncrementByReference(&$CountByReference)
    {
        ++$CountByReference;
        echo "<p> IncrementByReference() value is $CountByReference</p>";
    }

    IncrementByReference($n);
    echo " N after calling by reference:  $n";


    
?>