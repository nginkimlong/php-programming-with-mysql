<!DOCTYPE html>
<html>
<head>
	<title>Search student certificate</title>
	<style type="text/css">
		.form_input 
		{
			max-width: 300px;
			height: 25px;
			border-radius: 3px;
		}
		.btn_save
		{
			width: 120px;
			height: 35px;
			border-radius: 5px;
		}
	</style>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

	<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>

	<!-- <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script> -->
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>



</head>
<body>
	<div style="width: 100%; min-height: 150px; background-color: #f1f1f1; ">
		<br/>
		<form class="form-inline">
		  
		  <div class="form-group mx-sm-3 mb-2">
		    <label for="inputPassword2" class="sr-only"></label>
		    <input required="" type="text" class="form-control" id="key_word" name="key_word" placeholder="Enter text here...">
		  </div>
		  <button type="button" class="btn btn-primary mb-2" onclick="searchStudent()">Search</button>
		</form>
	</div>

	<div id="table_search"></div>


	<script type="text/javascript">
		function searchStudent()
		{
			var key_word = document.getElementById("key_word").value;
			if (key_word== "")
			{
				alert("please input text in search box.");
				return 0;
			}

			$.ajax({
				url: "search.php",
		    	type: "GET",
		    	data: {"key_word": key_word},
		    	success:function(data)
		    	{	
		    		
		    		document.getElementById('table_search').innerHTML = data;
		    	}, error: function(data)
		    	{
		    		console.log("error");
		    		console.log(error);
		    	}
		    });
		}
	</script>
</body>
</html>