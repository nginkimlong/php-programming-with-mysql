<!DOCTYPE html>
<html>
<head>
	
		<title>Registeration</title>
		<style type="text/css">
			
			body {
			  margin: 0;
			  padding: 0;
			  background-color: #17a2b8;
			  height: 100vh;
			}
			#login .container #login-row #login-column #login-box {
			  margin-top: 120px;
			  max-width: 650px;
			  height: 530px;
			  border: 1px solid #9C9C9C;
			  background-color: #EAEAEA;
			}
			#login .container #login-row #login-column #login-box #login-form {
			  padding: 20px;
			}
			#login .container #login-row #login-column #login-box #login-form #register-link {
			  margin-top: -85px;
			}
		
			.form_input 
			{
				max-width: 500px;
				height: 25px;
				border-radius: 3px;
			}
			.btn_save
			{
				width: 120px;
				height: 35px;
				border-radius: 5px;
			}

	</style>

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

	<!-- <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script> -->

	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
	</title>
</head>
<body>
    <?php
        // Initialize the session
        session_start();
         
        // Check if the user is logged in, if not then redirect to login page
        if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
            header("location: user_login.php");
            exit;
        }
    ?>

	<div id="login">
        <h3 class="text-center text-white pt-5">Create user</h3>
        <div class="container">
            <div id="login-row" class="row justify-content-center align-items-center">
                <div id="login-column" class="col-md-6">
                    <div id="login-box" class="col-md-12">

                        <form id="login-form" class="form" action="create_process.php" method="post">
                            <h3 class="text-center text-info">Create User</h3>
                            <div class="form-group">
                                <label for="username" class="text-info">Username:</label><br>
                                <input type="text" name="username" id="username" class="form-control" required="">
                            </div>
                            <div class="form-group">
                                <label for="email" class="text-info">Email:</label><br>
                                <input type="email" name="email" id="email" class="form-control" required="">
                            </div>
                            <div class="form-group">
                                <label for="password" class="text-info">Password:</label><br>
                                <input type="password" name="txt_password" id="txt_password" class="form-control" minlength="8" required="">
                                <label>password at least 8 character.</label>
                            </div>

                            <div class="form-group">
                                <label for="password" class="text-info">Confirm Password:</label><br>
                                <input onkeyup="checkPassword()" type="password" name="txt_confirm_password" id="txt_confirm_password" class="form-control" required="" minlength="8">
                                <label>password at least 8 character.</label>
                            </div>

                            <div class="form-group">
                                <input type="submit" id="btn_submit" name="submit" class="btn btn-info btn-md" value="submit" disabled>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
    	function checkPassword()
    	{

    		
    		var password = document.getElementById("txt_password").value;
            var confirmPassword = document.getElementById("txt_confirm_password").value;
         
            if (password != confirmPassword) {
            	document.getElementById("btn_submit").disabled = true;
            	document.getElementById("txt_confirm_password").style.border = "1px solid red;";
            	return 0;
            }
        	document.getElementById("btn_submit").disabled = false;
        	document.getElementById("txt_confirm_password").style.border = "1px solid blue;";
            return 1;
	        
    	}
    </script>

</body>
</html>