<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

	<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>

	<!-- <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script> -->
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</head>
<body>
	<?php 
		include('connection.php');
		$conn = Conn();
		$key_word = $_GET['key_word'];

		$student_id = $key_word[0];
		$first_name_kh = $key_word[1];
		$last_name_kh = $key_word[2];
		$first_name_en = $key_word[3];
		$last_name_en = $key_word[4];
		$b = false;

		$condition = "";
		if ($student_id != "" || $student_id != null)
		{
			$condition = " student_id = '$student_id'";
			$b = true;

		}

		if ($first_name_kh != "" || $first_name_kh != null)
		{
			if ($b==true){
				$condition = $condition." AND first_name_kh LIKE CONCAT('%$first_name_kh%')";
			}else{
				$condition = $condition." first_name_kh LIKE CONCAT('%$first_name_kh%')";
				$b = true;
			}
			
		}


		if ($last_name_kh != "" || $last_name_kh != null)
		{
			if ($b ==true){
				$condition =$condition. " AND ast_name_kh LIKE CONCAT('%$last_name_kh%')";
				
			}else{
				$condition =$condition. " last_name_kh LIKE CONCAT('%$last_name_kh%')";
				$b =true;
			}
			
		}

		if ($first_name_en !="" || $first_name_en !=null){
			if ($b ==true){
				$condition =$condition." AND first_name_en LIKE CONCAT('%$first_name_en%')";
			}else{
				$condition =$condition." first_name_en LIKE CONCAT('%$first_name_en%')";
				$b =true;
			}
		}
		if ($last_name_en !="" || $last_name_en !=null){
			if ($b ==true){
				$condition =$condition." AND last_name_en LIKE CONCAT('%$last_name_en%') ";
			}else{
				$condition =$condition." last_name_en LIKE CONCAT('%$last_name_en%') ";
			}
		}


		$sql = "SELECT * FROM tbl_student WHERE". $condition. "ORDER BY student_id DESC LIMIT 50";	
		
		$result = $conn->query($sql);
		?>
		<table class="table">
			<thead>
				<tr>
					<th>Student ID</th>
					<th>Name (Kh)</th>
					<th>Name (En)</th>
					<th>Photo</th>
					<th>Certificate</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				
		<?php
		if ($result->num_rows > 0) {
		    while($row = $result->fetch_assoc()) {
		    	?>
				<div class="modal fade bd-example-modal-lg" id="studentId<?php echo $row['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div class="modal-dialog modal-lg" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div class="modal-body">
				        	<img src="<?php echo $row['certificate']; ?>" class="img-fluid img-thumbnail" alt="Responsive image" style="width: 100%; height: 100%;">
				      </div>
				    </div>
				  </div>
				</div>

				<tr>
					<td><?php echo $row['student_id']; ?></td>
					<td><?php echo $row['first_name_kh']. ' ' . $row['last_name_kh'] ; ?></td>
					<td><?php echo $row['first_name_en']. ' ' . $row['last_name_en']; ?></td>
					<td> <img src="<?php echo $row['photo']; ?>" style="width: 50px; height: 50px;"/> </td>
					<td><a type="button" data-toggle="modal" data-target="#studentId<?php echo $row['id']; ?>" data-whatever="@mdo"><img src="<?php echo $row['certificate'] ?>" style="width:55px; height: 60px;"></a style="border:none !important; background-color: none !important;"></td>
					<td><a href="edit.php/?pk=<?php echo $row['id'] ?>"><button class="btn_save">Edit</button></a><a href="delete.php/?pk=<?php echo $row['id'] ?>" onClick="return confirm('Delete This account?')"><button class="btn_save">Delete</button></a></td>
				</tr>
				
		<?php
		    }
		}

	?>
		</tbody>

		</table>

</body>
</html>