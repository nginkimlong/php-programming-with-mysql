<?php 
	
	// Initialize the session
	session_start();
	 
	// Check if the user is logged in, if not then redirect to login page
	if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
	    header("location: user_login.php");
	    exit;
	}

	// Start connect to database
	include('connection.php');
	$conn = Conn();


	if(isset($_POST['submit']))
	{
		$user_name = $_POST['username'];
		$password = $_POST['txt_password'];
	    $confirm_password = $_POST['txt_confirm_password'];
	    $email = $_POST['email'];

	   
    	$h_password = password_hash($password, PASSWORD_DEFAULT);
    	$h_confirm_password = password_hash($confirm_password, PASSWORD_DEFAULT);

	    $s_search = "SELECT * FROM tbl_user WHERE username = '$user_name'";
		$result_search = $conn->query($s_search);
    
    	if ($result_search->num_rows > 0) {
    		echo 'This username already exist, please change username.';
    		die();
    	}

    	$sql = "INSERT INTO tbl_user (username, email, password, confirm_password, authentication) VALUES ('$user_name', '$email', '$h_password', '$h_confirm_password', 1)";

    	if ($conn->query($sql) === TRUE) {

	    ?>
		    <script type="text/javascript">
		    	alert("Account user created successfully.");
		    	window.open("user_login.php","_self")
		    </script>

			
		<?php

		} else {
		    echo "Error: " . $sql . "<br>" . $conn->error;
		}
	}


?>