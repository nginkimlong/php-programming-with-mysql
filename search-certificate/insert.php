<?php 
		// Initialize the session
		session_start();
		 
		// Check if the user is logged in, if not then redirect to login page
		if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
		    header("location: user_login.php");
		    exit;
		}

		include('connection.php');
		$conn = Conn();

		// Check directory for upload file.
		$target_dir = "uploads/";
		$target_file = $target_dir . basename($_FILES["photo"]["name"]);
		$uploadOk = 1;
		$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

		// Check directory for upload file.
		$certificate_target_dir = "certificate/";
		$certificate_target_file = $certificate_target_dir . basename($_FILES["certificate"]["name"]);
		$certificate_uploadOk = 1;
		$certificate_imageFileType = strtolower(pathinfo($certificate_target_file,PATHINFO_EXTENSION));

		if(isset($_POST['submit']))
		{    
			$student_id = $_POST['student_id'];
		    $first_name_kh = $_POST['first_name_kh'];
		    $last_name_kh = $_POST['last_name_kh'];
		    $first_name_en = $_POST['first_name_en'];
		    $last_name_en = $_POST['last_name_en'];
		    $sex = $_POST['sex'];
		    $nationality = $_POST['nationality'];
		    $photo_a = $_POST['photo'];
		    $certificate_a = $_POST['certificate'];

		    $s_search = "SELECT * FROM tbl_student WHERE student_id = $student_id";
		   
		    $result = $conn->query($s_search);
		    
	    	if ($result->num_rows > 0) {
	    		echo 'This student ID already exist, please change it.';
	    		die();
	    	}

		    if ($target_file == NULL OR $target_file ==NULL) 
		    {
		    	echo "Please make sure photo is existed.";
		    	die();
		    }
		    if ($certificate_target_file == NULL OR $certificate_target_file ==NULL) 
		    {
		    	echo "Please make sure photo is existed.";
		    	die();
		    }


		    // Block photo of student.
		    $check = getimagesize($_FILES["photo"]["tmp_name"]);
		    if($check !== false) {
		        // echo "File is an image - " . $check["mime"] . ".";
		        $uploadOk = 1;
		    } else {
		        echo "File is not an image.";
		        $uploadOk = 0;
		        die();
		    }
			
			// Check if file already exists
			if (file_exists($target_file)) {
			    echo "Sorry, file already exists.";
			    $uploadOk = 0;
			    die();
			}
			// Check file size
			if ($_FILES["photo"]["size"] > 500000) {
			    echo "Sorry, your file is too large.";
			    $uploadOk = 0;
			    die();
			}
			// Allow certain file formats
			if((string)$imageFileType != "jpg" && (string)$imageFileType != "png" && (string)$imageFileType != "jpeg" && (string)$imageFileType != "gif") {
			    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
			    $uploadOk = 0;
			    die();
			}
			// Check if $uploadOk is set to 0 by an error
			if ($uploadOk == 0) {
			    echo "Sorry, your file was not uploaded.";
			    die();
			// if everything is ok, try to upload file
			} else {
			    if (move_uploaded_file($_FILES["photo"]["tmp_name"], $target_file)) {
			        echo "The file ". basename( $_FILES["photo"]["name"]). " has been uploaded.";
			    } else {
			        echo "Sorry, there was an error uploading your file.";
			        die();
			    }
			}


			// block certificate photo
			$check = getimagesize($_FILES["certificate"]["tmp_name"]);
		    if($check !== false) {
		        // echo "File is an image - " . $check["mime"] . ".";
		        $certificate_uploadOk = 1;
		    } else {
		        echo "File is not an image.";
		        $certificate_uploadOk = 0;
		        die();
		    }
			
			// Check if file already exists
			if (file_exists($certificate_target_file)) {
			    echo "Sorry, file already exists.";
			    
			    $certificate_uploadOk = 0;
			    die();
			}
			// Check file size
			if ($_FILES["certificate"]["size"] > 500000) {
				
			    echo "Sorry, your file is too large.";
			    $certificate_uploadOk = 0;
			    die();
			}
			// Allow certain file formats
			if((string)$certificate_imageFileType != "jpg" && (string)$certificate_imageFileType != "png" && (string)$certificate_imageFileType != "jpeg" && (string)$certificate_imageFileType != "gif") {
			    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
			    $certificate_uploadOk = 0;
			    die();
			}
			// Check if $uploadOk is set to 0 by an error
			if ($certificate_uploadOk == 0) {
			    echo "Sorry, your file was not uploaded.";
			    die();
				// if everything is ok, try to upload file
			} else {
			    if (move_uploaded_file($_FILES["certificate"]["tmp_name"], $certificate_target_file)) {
			        echo "The file ". basename( $_FILES["certificate"]["name"]). " has been uploaded.";
			    } else {
			        echo "Sorry, there was an error uploading your file.";
			        die();
			    }
			}
 
		    $sql = "INSERT INTO tbl_student (student_id, first_name_kh, last_name_kh, first_name_en, last_name_en, sex, nationality, photo, certificate) VALUES ('$student_id', '$first_name_kh','$last_name_kh','$first_name_en', '$last_name_en', '$sex', '$nationality', '$target_file', '$certificate_target_file')";
		    
		    if ($conn->query($sql) === TRUE) {

		    ?>
			    <script type="text/javascript">
			    	alert("New record was saved.");
			    </script>
			
			<a href="student_admin.php"> <button> Back</button> </a>
			<?php

			} else {
			    echo "Error: " . $sql . "<br>" . $conn->error;
			}
		}
		else
		{
			echo "nothing on you";
		}
	?>