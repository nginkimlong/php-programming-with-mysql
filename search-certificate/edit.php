<!DOCTYPE html>
<html>
<head>
	<title>Edit</title>
	<style type="text/css">
		.form_input 
		{
			max-width: 300px;
			height: 25px;
			border-radius: 3px;
		}
		.btn_save
		{
			width: 120px;
			height: 35px;
			border-radius: 5px;
		}
	</style>
	<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>

</head>
<body>
	
	<?php 


		// Initialize the session
		session_start();
		 
		// Check if the user is logged in, if not then redirect to login page
		if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
		    header("location: user_login.php");
		    exit;
		}

		include('connection.php');
		$conn = Conn();

		$id = $_GET['pk'];
		$sql = "SELECT * FROM tbl_student WHERE id='$id'";
		$result = $conn->query($sql);

		?>
		<div style="text-align: center;">
			<h1> Edit student information with ID <?php echo $student_id; ?> </h1>
		</div>
		<?php 

		if ($result->num_rows > 0) {
    		while($row = $result->fetch_assoc()) {
    			?>
				<div style="background-color: #f1f1f1; padding-left: 20px; padding-right: 20px; padding-top: 10px; padding-bottom: 10px;">
					<form action="/search-certificate/edit_process.php" method="post" enctype="multipart/form-data">
						<input type=hidden id="id" name="id" value=<?php echo $row['id'] ?> />
							
						<table>
							<tr>
								<td>
									Student ID:
								</td>
								<td> 
									<input class="form_input" value="<?php echo $row['student_id'] ?>" type="text" id="student_id" name="student_id" placeholder="Student's ID" required="">  <span style="color: gray;">(Eg.001)</span>
								</td>

							</tr>
							<tr>
								<td>
									First Name (Kh):
								</td>
								<td> 
									<input class="form_input" type="text" id="first_name_kh" name="first_name_kh" placeholder="Name in khmer" required="" value="<?php echo $row['first_name_kh'] ?>" >
								</td>
							</tr>
							<tr>
								<td>Last Name (Kh): </td>
								<td>
									<input class="form_input" type="text" id="last_name_kh" name="last_name_kh"  placeholder="Name in khmer" required="" value="<?php echo $row['last_name_kh'] ?>">
								</td>
							</tr>
							<tr>
								<td>First Name (en): </td><td><input  class="form_input"type="text"  id = "first_name_en" name="first_name_en"  placeholder="Name in En" required="" value="<?php echo $row['first_name_en'] ?>"></td>
							</tr>
							<tr>
								<td>Last Name (en): </td><td><input  class="form_input" type="text" id = "last_name_en" name="last_name_en" placeholder="Name in En" required="" value="<?php echo $row['first_name_en'] ?>"></td>
							</tr>
							<tr>
								<td>Sex:</td>
								<td>
									 
									<select class="form_input" id="sex" name="sex">
									    <option value="male">Male</option>
									    <option value="female">Female</option>
									  </select>
								</td>
							</tr>
							<tr>
								<td>Nationality: </td><td><input class="form_input" type="text" placeholder="nationality" id="nationality" name="nationality" required="" value="<?php echo $row['nationality'] ?>"> </td>
							</tr>
							<tr>
								<td>
									<label for="photo">Choose a profile picture:</label> 
								</td>
								<td>
									<input type="hidden" value="<?php echo $row['photo']; ?>" name="h_photo" id="h_photo">
									<input class="form_input" type="file" id="photo" name="photo" accept="image/png, image/jpeg, image/jpg, image/gif" value="<?php echo $row['photo']; ?>">

								</td>
							</tr>
							<tr>
								<td>
									<label for="certificate">Choose a certificate picture:</label> </label>
								</td>
								<td>
									<input type="hidden" name = "h_certificate" value="<?php echo $row['certificate'] ?>">
									<input class="form_input" type="file" id="certificate" name="certificate" accept="image/png, image/jpeg, image/jpg, image/gif" value="<?php echo $row['certificate'] ?>">
									
								</td>
							</tr>
						</table>

						<div style="width: 90%; margin-top: 5px; margin-bottom: 5px; padding: 10px; background-color: white; ">
							<a href="/search-certificate/student_admin.php"  name="edit" class="btn_save">Cancel</a>  
							<input type="submit" class="btn_save" name="submit" value="Submit">
		
						</div>

					</form>
				</div>
    	<?php }
		}else{
			echo '<div style="text-align:center;"><h4>No record to edit.</h4></div>';
		}
	?>

	<script type="text/javascript">
		function updateRecord(){

			var student_id = document.getElementById('student_id').value;
			var first_name_kh = document.getElementById('first_name_kh').value;
			var last_name_kh = document.getElementById("last_name_kh").value;
			var first_name_en = document.getElementById("last_name_en").value;
			var last_name_en = document.getElementById("last_name_en").value;
			var sex = document.getElementById("sex").value;
			var nationality = document.getElementById("nationality").value;
			var photo = document.getElementById("photo").value;
			var certificate = document.getElementById("certificate").value;
			
			$.ajax({
	            type: "POST",
	            url: 'edit_process.php',
	            data: $(this).serialize(),
	            success: function(data)
	            {
	                console.log('success');
	                console.log(data);
	           	}, error: function(err)
	           	{
	           		console.log(err);
	           	}
       		});

		}
	</script>

</body>
</html>