

<!DOCTYPE html>
<html>
<head>
	<title>Administrator</title>
	<style type="text/css">
		.form_input 
		{
			max-width: 300px;
			height: 25px;
			border-radius: 3px;
		}
		.btn_save
		{
			width: 120px;
			height: 35px;
			border-radius: 5px;
		}
	</style>

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

	<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>

	<!-- <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script> -->
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</head>
<body>
	<?php 
		// Initialize the session
		session_start();
		 
		// Check if the user is logged in, if not then redirect to login page
		if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
		    header("location: user_login.php");
		    exit;
		}

	?>


	<div style="width: 100%; padding-left: 15px; padding-right: 15px;">
		<table style="width: 100%;">
			<tr>
				<td style="text-align: left;"><h1> Form of student's certificate </h1></td>
				<td style="text-align: right;"><a href="register.php">Create User</a> |  <a href="logout.php"> <span class="glyphicon glyphicon-asterisk"></span> Logout</td>
			</tr>
		</table>
		
	</div>
	<div style="background-color: #f1f1f1; padding-left: 20px; padding-right: 20px; padding-top: 10px; padding-bottom: 10px;">

		<form action="insert.php" method="post" enctype="multipart/form-data">
			<table>
				<tr>
					<td>
						Student ID:
					</td>
					<td> 
						<input class="form_input" type="text" id="student_id" name="student_id" placeholder="Student's ID" required="">  <span style="color: gray;">(Eg.001)</span>
					</td>

				</tr>
				<tr>
					<td>
						First Name (Kh):
					</td>
					<td> 
						<input class="form_input" type="text" id="first_name_kh" name="first_name_kh" placeholder="Name in khmer" required="">
					</td>
				</tr>
				<tr>
					<td>Last Name (Kh): </td>
					<td>
						<input class="form_input" type="text" id="last_name_kh" name="last_name_kh"  placeholder="Name in khmer" required="">
					</td>
				</tr>
				<tr>
					<td>First Name (en): </td><td><input  class="form_input"type="text"  id = "first_name_en" name="first_name_en"  placeholder="Name in En" required=""></td>
				</tr>
				<tr>
					<td>Last Name (en): </td><td><input  class="form_input" type="text" id = "last_name_en" name="last_name_en" placeholder="Name in En" required=""></td>
				</tr>
				<tr>
					<td>Sex:</td>
					<td>
						 
						<select class="form_input" id="sex" name="sex">
						    <option value="male">Male</option>
						    <option value="female">Female</option>
						  </select>
					</td>
				</tr>
				<tr>
					<td>Nationality: </td><td><input class="form_input" type="text" placeholder="nationality" id="nationality" name="nationality" required=""> </td>
				</tr>
				<tr>
					<td>
						<label for="photo">Choose a profile picture:</label> 
					</td>
					<td>
						<input required="" class="form_input" type="file" id="photo" name="photo" accept="image/png, image/jpeg, image/jpg, image/gif, image/PNG, image/JPEG, image/JPG, image/GIF">
					</td>
				</tr>
				<tr>
					<td>
						<label for="certificate">Choose a certificate picture:</label> </label>
					</td>
					<td>
						<input required="" class="form_input" type="file" id="certificate" name="certificate" accept="image/png, image/jpeg, image/jpg, image/gif">
					</td>
				</tr>
			</table>

			<div style="width: 90%; margin-top: 5px; margin-bottom: 5px; padding: 10px; background-color: white; ">
				<input type="submit" class="btn_save btn btn-primary mb-2" name="submit" value="Submit"> 
				<input type="button" class="btn_save btn btn-success mb-2" onclick="adminSearch();" value="Search">
			</div>
		</form>
	</div>


	<?php 

		// Start connection
		include('connection.php');
		$conn = Conn();
		$sql = "SELECT * FROM tbl_student ORDER BY student_id DESC LIMIT 30";

		$result = $conn->query($sql);

		

	?>

	<div style="width: 100%;padding:10px;" id="table_search">
		
		<table class="table" border="1">
			<thead>
				<tr>
					<th>ID</th>
					<th>Name (KH)</th>
					<th>Name (Kh)</th>
					<th>Photo </th>
					<th>Certificate</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php 
					if ($result->num_rows > 0) {
					    while($row = $result->fetch_assoc()) {
			  	?>

			  			<div class="modal fade bd-example-modal-lg" id="studentId<?php echo $row['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						  <div class="modal-dialog modal-lg" role="document">
						    <div class="modal-content">
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						          <span aria-hidden="true">&times;</span>
						        </button>
						      </div>
						      <div class="modal-body">
						        	<img src="<?php echo $row['certificate']; ?>" class="img-fluid img-thumbnail" alt="Responsive image" style="width: 100%; height: 100%;">
						      </div>
						    </div>
						  </div>
						</div>
				
		  				<input type=hidden id="id" name="id" value=<?php echo $row['id'] ?> />
						<tr style="text-align: left;">
							<td> <?php echo $row['student_id'] ?> </td>
							<td> <?php echo $row['first_name_kh'].' '. $row['last_name_kh']; ?> </td>
							<td> <?php echo $row['first_name_en'].' '.$row['last_name_en']; ?> </td>
							<td> <img src="<?php echo $row['photo']; ?>" style="width:55px; height: 60px;"/> </td>
							

							<td><a type="button" data-toggle="modal" data-target="#studentId<?php echo $row['id']; ?>" data-whatever="@mdo"><img src="<?php echo $row['certificate'] ?>" style="width:55px; height: 60px;"></a style="border:none !important; background-color: none !important;"></td>
							<td><a href="edit.php/?pk=<?php echo $row['id'] ?>"><button class="btn_save">Edit</button></a><a href="delete.php/?pk=<?php echo $row['id'] ?>" onClick="return confirm('Delete This account?')"><button class="btn_save">Delete</button></a></td>
						</tr>
				<?php    
					}
					} else {
					    echo "0 results";
					}
				?>
				
			</tbody>
		</table>
	</div>

	<script type="text/javascript">
		function adminSearch()
		{
			var student_id = document.getElementById('student_id').value;
			var first_name_kh = document.getElementById('first_name_kh').value;
			var last_name_kh = document.getElementById('last_name_kh').value;
			var first_name_en = document.getElementById('first_name_en').value;
			var last_name_en = document.getElementById('last_name_en').value;
				
			if (student_id == "" && first_name_kh == "" && last_name_kh == "" && first_name_en == "" && last_name_en == "")
			{
				alert('Please input text in a box before searching.');
				return;
			}
				
			var key_word = [student_id, first_name_kh, last_name_kh, first_name_en, last_name_en];
			// var key_word = student_id;
			$.ajax({
				url: "admin_search_processing.php",
		    	type: "GET",
		    	data: {"key_word": key_word},
		    	success:function(data)
		    	{	
		    		document.getElementById('table_search').innerHTML = data;
		    	}, error: function(error)
		    	{
		    		console.log("error");
		    		console.log(error);
		    	}
		    });
		
		}
	</script>

</body>
</html>